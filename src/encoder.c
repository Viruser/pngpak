/*
 * pngpak
 * This program will pack a binary file in form of a png image
 * Copyright(C) 2015 lowin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "pngpak.h"
#include <math.h>
#include <png.h>

int pngpak_encode(FILE *infp, uint64_t inputsize, FILE *outfp)
{
    png_voidp error_ptr = NULL;
    png_uint_32 width, height;
    width = ceil(sqrt((long double)inputsize / 8));
    height = ceil((long double)inputsize / (long double) (width * 8 )) + 1;
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, error_ptr, pngpak_error, pngpak_warn);
    png_infop info_ptr = png_create_info_struct(png_ptr);
    png_init_io(png_ptr, outfp);
    png_set_IHDR(png_ptr, info_ptr, width, height, 16, PNG_COLOR_TYPE_RGB_ALPHA,
      PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);
    png_byte *buffer = (png_byte*)malloc(width * 8);
    png_bytep row_ptr = buffer;
    memset(buffer, 0, width * 8);
    sprintf((char*)buffer, "%"PRIu64, inputsize);
    png_write_rows(png_ptr, &row_ptr, 1);
    int i;
    for (i = 0; i < height; i++)
        {
            fread(buffer, 1, width * 8, infp);
            png_write_rows(png_ptr, &row_ptr, 1);
        }
    png_write_end(png_ptr, info_ptr);

    return 0;

}


void pngpak_error()
{
    fprintf(stderr, "error\n");
    exit(1);
}

void pngpak_warn()
{
    fprintf(stderr, "warn\n");
}

