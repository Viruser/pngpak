/*
 * pngpak
 * This program will pack a binary file in form of a png image
 * Copyright(C) 2015 lowin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "pngpak.h"
#include <png.h>

int pngpak_decode(FILE *infp, FILE *outfp)
{
    png_voidp error_ptr = NULL;
    png_uint_32 width, height;
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, error_ptr, pngpak_error, pngpak_warn);
    png_infop info_ptr = png_create_info_struct(png_ptr);

    png_init_io(png_ptr, infp);
    png_read_info(png_ptr, info_ptr);
    //png_get_IHDR(png_ptr, info_ptr, &width, &height, NULL, NULL, NULL, NULL, NULL);
    width = png_get_image_width(png_ptr, info_ptr);
    height = png_get_image_height(png_ptr, info_ptr);
    png_byte *buffer = (png_byte*)malloc(width * 8);
    png_bytep row_ptr = buffer;

    png_read_rows(png_ptr, &row_ptr, NULL, 1);


    uint64_t filesize = 0;
    sscanf((char*)buffer, "%"PRIu64, &filesize);
    int i;
    for(i = 1; i < height; i++)
    {
        png_read_rows(png_ptr, &row_ptr, NULL, 1);

        if(filesize >= width * 8)
            fwrite(buffer, 1, width * 8, outfp);
        else
        {
            fwrite(buffer, 1, filesize, outfp);
            break;
        }
        filesize -= width * 8;
    }
    return 0;
}
