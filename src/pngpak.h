/*
 * pngpak
 * This program will pack a binary file in form of a png image
 * Copyright(C) 2015 lowin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#define PROGRAM_VERSION "0.1-pre_alpha"

#define PNGBIN_MODE_DECODE -1
#define PNGBIN_MODE_ENCODE 1

int pngpak_encode(FILE *infp,uint64_t inputsize, FILE *outfp);
int pngpak_decode(FILE *infp, FILE *outfp);

void pngpak_error();
void pngpak_warn();
