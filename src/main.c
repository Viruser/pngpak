/*
 * pngpak
 * This program will pack a binary file in form of a png image
 * Copyright(C) 2015 lowin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "pngpak.h"
#include <getopt.h>
#include <sys/stat.h>

struct arguments
{
    int mode;
    char* output;
    char* input;
};

inline struct arguments *parse_options(int, char**);
uint64_t getfilesize(char* file);
void usage(int, char*);
void version();

int main(int argc, char** argv)
{
    struct arguments *x = parse_options(argc, argv);
    FILE *infp;
    FILE *outfp;
    unsigned long long fsize = 0;
    if(strcmp(x->input, "-") == 0)
    {
        infp = stdin;
    }
    else
    {
        infp = fopen(x->input, "rb");
        if(!infp)
        {
            fprintf(stderr, "Error openning file %s for reading.\n", x->input);
            exit(EXIT_FAILURE);
        }
        fsize = getfilesize(x->input);
    }
    if(strcmp(x->output, "-") == 0)
        outfp = stdout;
    else
    {
        outfp = fopen(x->output, "wb");
        if(!outfp)
        {
            fprintf(stderr, "Error openning file %s for writing.\n", x->output);
            exit(EXIT_FAILURE);
        }
    }
    if(x->mode == PNGBIN_MODE_DECODE)
    {
        pngpak_decode(infp, outfp);
    }
    else if(x->mode == PNGBIN_MODE_ENCODE)
    {
        pngpak_encode(infp, fsize, outfp);
    }
    return 0;
}

inline struct arguments *parse_options(int argc, char** argv)
{
    int c;
    struct arguments *ret = (struct arguments*) malloc(sizeof(struct arguments));
    ret->mode = PNGBIN_MODE_DECODE;
    struct option long_option[] =
    {
        { "encode", no_argument, 0, 'e'},
        { "decode", no_argument, 0, 'd'},
        { "output", required_argument, 0, 'o'},
        { "version", no_argument, 0, 'v'},
        { "help", no_argument, 0, 'h'},
        { 0, 0, 0, 0 }
    };
    while(1)
    {
        int option_index = 0;
        c = getopt_long(argc, argv, "edo:vh", long_option, &option_index);
        if(c == -1)
            break;
        switch(c)
        {
            case 'e':
                ret->mode = PNGBIN_MODE_ENCODE;
                break;
            case 'd':
                ret->mode = PNGBIN_MODE_DECODE;
                break;
            case 'o':
                ret->output = (char*)malloc(strlen(optarg));
                strncpy(ret->output, optarg, strlen(optarg));
                break;
            case 'v':
                version();
                break;
            case 'h':
                usage(EXIT_SUCCESS, argv[0]);
                break;
            case '?':
                break;
        }
    }
    if(optind == argc - 1)
    {
        ret->input = (char*) malloc(strlen(argv[optind]) + 1);
        strncpy(ret->input, argv[optind], strlen(argv[optind]) + 1);
    }
    else
    {
        usage(EXIT_FAILURE, argv[0]);
    }
    if(ret->input == NULL)
    {
        ret->input = (char*)malloc(sizeof(char) * 2);
        ret->input = "-";
    }
    if(ret->output == NULL)
    {
        ret->output = (char*)malloc(sizeof(char) * 2);
        ret->output = "-";
    }
    return ret;
}

uint64_t getfilesize(char* file)
{
    struct stat *ststruct = (struct stat*) malloc(sizeof (struct stat));
    stat(file, ststruct);
    return ststruct->st_size;

}

void usage(int exitcode, char* name)
{
    fprintf(stderr, "USAGE: %s [OPTIONS] INPUT_FILE\n"
            "Package a binary file in form of a PNG image\n\n"
            "Options:\n"
            "-d, --decode\t\tDecode data from a PNG image\n"
            "-e, --encode\t\tEncode data into a PNG image\n"
            "-o FILE, --output FILE\tUse FILE as output instead of stdout\n"
            "-v, --version\t\tShow version\n"
            "-h, --help\t\tShow this help\n\n"
            "If no options are given, decode is assumed.\nIf no INPUT_FILE is specified, stdin will be used\n", name);
    exit(exitcode);
}
void version()
{
    fprintf(stdout, "pngpak %s\nPack binary files into a png image\n\n"
            "Copyright (C) lowin\nLicense GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n"
            "This is free software, you are free to modify and redistribute it.\n"
            "This software is provided AS IS, there is no warranty. Use at your own risk.\n", PROGRAM_VERSION);
    exit(0);
}

