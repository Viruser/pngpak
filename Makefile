CC = gcc
LD = gcc

SRCDIR = src
BINDIR = bin
BINFILE = pngpak
OBJDIR = obj

CFLAGS = -Wall
LDFLAGS = -lpng -lm

all: release

release: src/main.c src/encoder.c src/decoder.c
	test -d $(BINDIR) || mkdir -p $(BINDIR)
	test -d $(OBJDIR) || mkdir -p $(OBJDIR)
	$(CC) $(CFLAGS) -c $(SRCDIR)/main.c -o $(OBJDIR)/main.o
	$(CC) $(CFLAGS) -c $(SRCDIR)/encoder.c -o $(OBJDIR)/encoder.o
	$(CC) $(CFLAGS) -c $(SRCDIR)/decoder.c -o $(OBJDIR)/decoder.o
	$(LD) $(OBJDIR)/main.o $(OBJDIR)/encoder.o $(OBJDIR)/decoder.o $(LDFLAGS) -o $(BINDIR)/$(BINFILE)

clean:
	rm -rf $(BINDIR)
	rm -rf $(OBJDIR)

install:
	install $(BINDIR)/$(BINFILE) /usr/bin